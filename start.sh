#!/bin/bash
modprobe overlay
mkdir -p /var/run/docker/containerd
mkdir -p /run/containerd/io.containerd.runtime.v1.linux/
mkdir -p /var/lib/containerd/io.containerd.runtime.v1.linux/
mkdir -p /var/run/docker/netns
mkdir -p /run/user/0
touch /var/run/docker/netns/default

mount --bind /proc/1/ns/net  /var/run/docker/netns/default
insmod qtfs_server.ko qtfs_server_ip=0.0.0.0 qtfs_log_level=INFO
./engine 4096 16
