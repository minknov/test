# 1 容器管理面无感卸载
## 1.1 简介
容器管理面，即dockerd、containerd、isulad等容器的管理工具，而容器管理面卸载，即是将容器管理面卸载到与容器所在机器（以下称为host）之外的另一台机器（以下称为dpu）上运行。

我们使用了qtfs将host的一些与容器运行相关的目录挂载到dpu上，使得容器管理面工具可以访问到这些目录，为容器准备运行所需要的环境，此处，因为需要挂载远端的proc和sys，所以，我们创建了一个专门的rootfs以作为dockerd、contianerd的运行环境（以下称为`/another_rootfs`）。

并且通过rexec执行容器的拉起、删除等操作，使得可以将容器管理面和容器分离在不同的两台机器上，远程对容器进行管理。
## 1.2 相关组件介绍
### 1.2.1 rexec介绍
rexec是一个用go语言开发的远程执行工具，可用来远程执行对端服务器的命令；
### 1.2.2 dockerd相关改动介绍
对dockerd的改动是基于18.09版本的。

在containerd中，暂时注释掉了通过hook调用libnetwork-setkey的部分，此处不影响容器的拉起。并且，为了docker load的正常使用，注释掉了在mounter_linux.go 中mount函数中一处错误的返回。

最后，因为在容器管理面的运行环境中，将`/proc`挂在了host的proc文件系统，而本地的proc文件系统则挂载在了`/local_proc`，所以，dockerd以及containerd中的对`/proc/self/xxx`或者`/proc/getpid()/xxx`或者相关的文件系统访问的部分，我们统统将`/proc`改为了`/local_proc`。
### 1.2.3 containerd相关改动介绍
对于containerd的改动是基于containerd-1.2-rc.1版本的。

在获取mountinfo时，因为`/proc/self/mountinfo`只能获取到dockerd本身在本地的mountinfo，而无法获取到host的mountinfo，所以，将其改为了`/proc/1/mountinfo`，使其通过获取host1号进程mountinfo的方式得到host的mountinfo。

在contaienrd-shim中，将与containerd通信的unix socket改为了用tcp通信，containerd通过`SHIM_HOST`环境变量获取containerd-shim所运行环境的ip，即hostip。用shim的哈希值计算出一个端口号，并以此作为通信的端口，来拉起containerd-shim.

并且，将原来的通过系统调用给contaienr-shim发信号的方式，改为了通过远程调用kill指令的方式向shim发信号，确保了docker杀死容器的行为可以正确的执行。


# 2 操作环境

物理机操作系统: openEuler 22.03 LTS

Docker版本: docker-18.09版本

containerd-1.2.0版本

文件下载列表：

- 1 放在DPU & HostOS

https://gitee.com/minknov/test.git :本仓库中的rexec、rexec_server
- 2 放在DPU上即可
https://gitee.com/minknov/test.git :本仓库中的 qtfs.ko、
docker-engine-18.09.0-312.x86_64.rpm、containerd-1.2.0-304.x86_64.rpm
openEuler官方repo中的[iptables](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/iptables-1.8.7-5.oe2203.x86_64.rpm), [libtool](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/libtool-2.4.6-34.oe2203.x86_64.rpm), [emacs](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/emacs-27.2-3.oe2203.x86_64.rpm), [autoconf](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/autoconf-2.71-2.oe2203.noarch.rpm), [automake](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/automake-1.16.5-3.oe2203.noarch.rpm), [libtool-ltdl](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/libtool-ltdl-2.4.6-34.oe2203.x86_64.rpm), [m4](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/m4-1.4.19-2.oe2203.x86_64.rpm)， [tar](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/tar-1.34-1.oe2203.x86_64.rpm), [libcgroup](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/libcgroup-0.42.2-1.oe2203.x86_64.rpm), [qemu-img](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/qemu-img-6.2.0-29.oe2203.x86_64.rpm), []

- 3 放在host上
https://gitee.com/minknov/test.git :本仓库中的engine、qtfs_server.ko、docker-engine-18.09.0-312.x86_64.rpm、containerd-1.2.0-304.x86_64.rpm





# 3 容器管理面卸载操作指南

> 说明：
>
> 1. 在host端和dpu端，都要拉起rexec_server；Host侧可使用 [dpu IP]:[dpu rexec端口号] 远程操作DPU上的二进制，反之亦然；
> 2. host端拉起rexec_server，主要是用于dpu创建容器时用rexec拉起containerd-shim；
> 3. dpu拉起rexec_server，则是为了执行containerd-shim对dockerd和containerd的调用。

## 3.1 拉起rexec_server

### 3.1.1 拷贝二进制文件

需要将本仓库中的rexec_server拷贝到dpu和host上

``` bash
cp rexec_server /usr/bin/
chmod +x rexec_server
```
### 3.1.2 配置rexec_server服务

为方便起见，可以将rexec_server作为一个systemd服务；

1. 同时在DPU和Host上的`/usr/lib/systemd/system/`目录下，添加`rexec.service`服务文件，内容如下：

`/usr/lib/systemd/system/rexec.service`，service文件中中的<端口号>根据用户的意愿自行分配

    [Unit]
    Description=Rexec_server Service
    After=network.target

    [Service]
    Type=simple
    Environment=CMD_NET_ADDR=tcp://0.0.0.0:<端口号>
    ExecStart=/usr/bin/rexec_server
    ExecReload=/bin/kill -s HUP $MAINPID
    LimitNOFILE=1048576
    LimitNPROC=infinity
    LimitCORE=infinity
    TimeoutStartSec=0
    Delegate=yes
    KillMode=process
    Restart=on-failure
    StartLimitBurst=3
    StartLimitInterval=60s
    TimeoutStopSec=10

    [Install]
    WantedBy=multi-user.target


2. 通过systemctl start rexec，拉起rexec server服务

``` bash
systemctl daemon-reload
systemctl enable --now rexec
```


### 3.1.2 rexec操作示例

配置好rexec_server服务后，如果要在dpu上调用host上的二进制，可先将`rexec`拷贝到`/usr/bin`下面，再使用如下命令远程执行对端服务器上的二进制；

``` bash
CMD_NET_ADDR=tcp://<host ip>:<host端rexec_server端口号> rexec [要执行的指令]
```

例如，在DPU上操作Host（假设ip为192.168.1.1，rexec端口号为6666）上的ls指令：

``` bash
CMD_NET_ADDR=tcp://192.168.1.1:6666 rexec /usr/bin/ls
```

> 补充说明：
>
> 如果不想systemd服务的形式拉起rexec_server，可使用命令手动拉起。
>
> 例如，在host端执行`CMD_NET_ADDR=tcp://0.0.0.0:<端口号> rexec_server` 命令，可拉起rexec_server进程；



## 3.2 准备dockerd和containerd运行的rootfs
> 注：本步骤仅需在dpu上执行的

在下面的文档中，我们将这个rootfs称为`/another_rootfs`（具体的目录名称，可以根据自己的需求进行调整）

rootfs可使用如下3.2.1或3.2.2两种方案之一，推荐使用openEuler官方qcow2镜像。在准备好rootfs后，按照3.2.3中的方式，向/another_rootfs中安装软件包

### 3.2.1 拷贝根目录

一般来说，只需要将根目录直接拷贝到这个文件夹即可

请使用如下命令操作拷贝动作

``` bash
mkdir /another_rootfs
cp -r /usr /another_rootfs
cp -r /sbin /another_rootfs
cp -r /bin /another_rootfs
cp -r /lib64 /another_rootfs
cp -r /lib /another_rootfs
mkdir /another_rootfs/boot
mkdir /another_rootfs/dev
mkdir /another_rootfs/etc
mkdir /another_rootfs/home
mkdir /another_rootfs/mnt
mkdir /another_rootfs/opt
mkdir /another_rootfs/proc
mkdir /another_rootfs/root
mkdir /another_rootfs/run
mkdir /another_rootfs/var
mkdir /another_rootfs/etc
mkdir /another_rootfs/sys
mkdir /another_rootfs/local_proc
```

### 3.2.2 使用openEuler官方qcow2镜像

如果根目录并非完全干净的新环境，可以使用openEuler官方提供的qcow2镜像，来准备一个新的rootfs：

#### 3.2.2.1 工具安装

需要用yum安装xz、kpartx、qemu-img

``` bash
yum install xz kpartx qemu-img
```

#### 3.2.2.2 下载qcow2镜像

在openEuler官网获取[22.03版本openEuler-x86虚拟机镜像](https://repo.openeuler.org/openEuler-22.03-LTS/virtual_machine_img/x86_64/openEuler-22.03-LTS-x86_64.qcow2.xz)(X86架构)，或者[22.03版本openEuler-arm64虚拟机镜像](https://repo.openeuler.org/openEuler-22.03-LTS/virtual_machine_img/aarch64/openEuler-22.03-LTS-aarch64.qcow2.xz)（ARM架构）。

#### 3.2.2.3 解压qcow2镜像

使用xz -d解压为openEuler-22.03-LTS-<arch>.qcow2文件, 以x86镜像为例

``` bash
xz -d openEuler-22.03-LTS-x86_64.qcow2.xz
```
#### 3.2.2.4 挂载qcow2镜像并拷贝文件

1. 使用`modprobe nbd maxpart=<任意数字>`来加载nbd模块，
2. `qemu-nbd -c /dev/nbd0 <虚拟机镜像的路径>`
3. 创建任意文件夹`/random_dir`, 
4. 执行挂载`mount /dev/nbd0p2 /random_dir`
5. 拷贝文件
``` bash
mkdir /another_rootfs
cp -r /random_dir/* /another_rootfs/
```

此时，虚拟机镜像遍已经挂载到当前文件夹中了；

#### 3.2.2.5 qcow2卸载
在准备好rootfs后，需要卸载qcow2文件，此时，需要执行如下指令：
``` bash
umount /random_dir
qemu-nbd -d /dev/nbd0
```

### 3.2.3 向another_rootfs中安装软件
1. 将根目录中的`/etc/resolv.conf`拷贝到`/another_rootfs/etc/resolv.conf`
2. 清空`/another_rootfs/etc/yum.repo.d`中的文件，并将`/etc/yum.repo.d/`中的文件拷贝到`/another_rootfs/etc/yum.repo.d`
3. 使用`yum install <软件包>  --installroot=/another_rootfs`来安装软件包
```
yum install --installroot=/another_rootfs iptables
```

## 3.3 在Host侧拉起qtfs_server

需要将rexec、containerd-shim、runc、engine拷贝到`/usr/bin`下面，这里要注意权限问题，rexec、engine已提供，containerd在test-master所提供的rpm包[
containerd-1.2.0-304.x86_64.rpm
](https://gitee.com/minknov/test/blob/master/containerd-1.2.0-304.x86_64.rpm)中；runc可以下载[docker-runc软件包](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/docker-runc-1.0.0.rc3-300.oe2203.x86_64.rpm)；

通过rpm2cpio指令解压，并拷贝到usr/bin下面

``` bash
rpm2cpio docker-runc-1.0.0.rc3-300.oe2203.x86_64.rpm | cpio -div
cp ./usr/local/bin/runc /usr/bin
rpm2cpio containerd-1.2.0-304.x86_64.rpm | cpio -div
cp ./usr/bin/containerd-shim /usr/bin/
```

### 3.3.1 插入qtfs_server驱动

创建容器管理面所需要的文件夹，然后插入qtfs_server.ko，并拉起engine进程。

可以用如下脚本来执行此操作，如果执行错误，可能需要dos2unix来将此脚本的格式转换（如下所有脚本皆同理）。注意，在最后两行，需要将qtsf_server.ko和engine的路径填写为存放这两个组件的路径

``` bash
#!/bin/bash
modprobe overlay
mkdir /var/lib/docker/containers
mkdir -p /var/lib/docker/containers
mkdir -p /var/lib/docker/containerd
mkdir -p /var/lib/docker/overlay2
mkdir -p /var/lib/docker/tmp
mkdir -p /var/lib/docker/image
mkdir -p /var/run/docker/containerd
mkdir -p /run/containerd/io.containerd.runtime.v1.linux/
mkdir -p /var/run/docker/netns
mkdir -p /var/lib/containerd/io.containerd.runtime.v1.linux/
mkdir -p /run/user/0

touch /var/run/docker/netns/default

mount --bind /proc/1/ns/net  /var/run/docker/netns/default
insmod <ko路径>/qtfs_server.ko qtfs_server_ip=0.0.0.0 qtfs_log_level=INFO #此处需要自行修改ko的路径
<engine路径>/engine 4096 16 #此处需要自行修改engine的路径
```
此外在host端，还需要创建执行rexec指令的脚本`/usr/bin/dockerd`以及`/usr/bin/containerd`：

/usr/bin/dockerd：

``` bash
#!/bin/bash
CMD_NET_ADDR=tcp://<dpuip>:<rexec端口号> rexec /usr/bin/dockerd $*
```
/usr/bin/containerd：

``` bash
#!/bin/bash
CMD_NET_ADDR=tcp://<dpuip>:<rexec端口号> rexec /usr/bin/containerd $*
```

在创建完成后，需要用chmod为这两个脚本赋予执行权限
``` bash
chmod +x /usr/bin/containerd
chmod +x /usr/bin/dockerd
```

## 3.4 挂载Host上依赖目录至DPU
### 3.4.1 安装软件包

#### 3.4.2.1 在根目录的安装

1. DPU根目录中（another_rootfs之外）：安装iptables和libtool、libcgroup、tar,可以通过yum直接直接安装。
```
yum install iptables libtool libcgroup tar
```
也可以下载其所有的依赖包之后，用rpm指令安装，iptables以及libtool的包及依赖包链接如下：[iptables](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/iptables-1.8.7-5.oe2203.x86_64.rpm), [libtool](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/libtool-2.4.6-34.oe2203.x86_64.rpm), [emacs](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/emacs-27.2-3.oe2203.x86_64.rpm), [autoconf](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/autoconf-2.71-2.oe2203.noarch.rpm), [automake](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/automake-1.16.5-3.oe2203.noarch.rpm), [libtool-ltdl](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/libtool-ltdl-2.4.6-34.oe2203.x86_64.rpm), [m4](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/m4-1.4.19-2.oe2203.x86_64.rpm)， [tar](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/tar-1.34-1.oe2203.x86_64.rpm), [libcgroup](https://repo.openeuler.org/openEuler-22.03-LTS/everything/x86_64/Packages/libcgroup-0.42.2-1.oe2203.x86_64.rpm)。

在下载过上述软件包之后，执行命令
``` bash
rpm -ivh iptables-1.8.7-5.oe2203.x86_64.rpm libtool-2.4.6-34.oe2203.x86_64.rpm emacs-27.2-3.oe2203.x86_64.rpm autoconf-2.71-2.oe2203.noarch.rpm automake-1.16.5-3.oe2203.noarch.rpm libtool-ltdl-2.4.6-34.oe2203.x86_64.rpm m4-1.4.19-2.oe2203.x86_64.rpm tar-1.34-1.oe2203.x86_64.rpm libcgroup-0.42.2-1.oe2203.x86_64.rpm
```

#### 3.4.2.2 another_rootfs环境配置

1、在`/another_rootfs`中，需要安装iptables，这个是dockerd启动所必须的依赖；

使用`yum install <软件包>  --installroot=/another_rootfs`来安装软件包

2、并需要将rexec拷贝到`/another_rootfs/usr/bin`下面，并对其添加可执行权限

``` bash
cp rexec /another_rootfs/usr/bin
chmod +x /another_rootfs/usr/bin/rexec
```
3、另外，需要将我们提供的containerd和docker-engine两个rpm包解压之后，将其中的containerd和dockerd拷贝到`/another_rootfs/usr/bin`下面, 将docker拷贝到`/usr/bin`下面

``` bash
rpm2cpio containerd-1.2.0-304.x86_64.rpm | cpio -div
cp usr/bin/containerd /another_rootfs/usr/bin
rpm2cpio docker-engine-18.09.0-312.x86_64.rpm | cpio -div
cp usr/bin/dockerd /another_rootfs/usr/bin
cp usr/bin/docker /usr/bin
cp usr/bin/runc /another_rootfs/usr/bin/
```
4、在`/another_rootfs`中，需要创建如下脚本，需要先删掉/another_rootfs/usr/sbin/modprobe
``` bash
rm -f /another_rootfs/usr/sbin/modprobe
```

containerd-shim创建路径：`/another_rootfs/usr/local/bin/containerd-shim`

``` bash
#!/bin/bash
CMD_NET_ADDR=tcp://<hostip>:<rexec端口号> /usr/bin/rexec /usr/bin/containerd-shim $*
```

remote_kill创建路径：`/another_rootfs/usr/bin/remote_kill`

``` bash
#!/bin/bash
CMD_NET_ADDR=tcp://<hostip>:<rexec端口号> /usr/bin/rexec /usr/bin/kill $*
```

modprobe创建路径：`/another_rootfs/usr/sbin/modprobe`

``` bash
#!/bin/bash
CMD_NET_ADDR=tcp://<hostip>:<rexec端口号> /usr/bin/rexec /usr/sbin/modprobe $*
```
在创建完成后，为其赋予执行权限
``` bash
chmod +x /another_rootfs/usr/local/bin/containerd-shim
chmod +x /another_rootfs/usr/bin/remote_kill
chmod +x /another_rootfs/usr/sbin/modprobe
```
#### 3.4.2.3 目录挂载
在dpu上执行如下的脚本，将dockerd、containerd所需要的host目录挂载到dpu。

并且，需要确保在以下脚本 prepare.sh 中被挂载的远程目录在host和dpu都存在。 

``` bash
#!/bin/bash
mkdir -p /another_rootfs/var/run/docker/containerd
iptables -t nat -N DOCKER
echo "---------insmod qtfs ko----------"
insmod <qtfs.ko的路径>/qtfs.ko qtfs_server_ip=<host ip> qtfs_log_level=INFO #此处需要自行修改ip, 以及ko的路径

mkdir /another_rootfs/local_proc/
mount -t proc proc /another_rootfs/local_proc/
mount --bind /var/run/ /another_rootfs/var/run/
mount --bind /var/lib/ /another_rootfs/var/lib/
mount --bind /etc /another_rootfs/etc

mount -t devtmpfs devtmpfs /another_rootfs/dev/

mount -t sysfs sysfs /another_rootfs/sys
mkdir -p /another_rootfs/sys/fs/cgroup
mount -t tmpfs tmpfs /another_rootfs/sys/fs/cgroup
list="perf_event freezer files net_cls,net_prio hugetlb pids rdma cpu,cpuacct memory devices blkio cpuset"
for i in $list
do
        echo $i
        mkdir -p /another_rootfs/sys/fs/cgroup/$i
        mount -t cgroup cgroup -o rw,nosuid,nodev,noexec,relatime,$i /another_rootfs/sys/fs/cgroup/$i
done

mount -t qtfs -o proc /proc /another_rootfs/proc
echo "proc"
mount -t qtfs /sys /another_rootfs/sys
echo "cgroup"

mkdir -p /another_rootfs/var/lib/docker/containers
mkdir -p /another_rootfs/var/lib/docker/containerd
mkdir -p /another_rootfs/var/lib/docker/overlay2
mkdir -p /another_rootfs/var/lib/docker/image
mkdir -p /another_rootfs/var/lib/docker/tmp
mount -t qtfs /var/lib/docker/containers /another_rootfs/var/lib/docker/containers
mount -t qtfs /var/lib/docker/containerd /another_rootfs/var/lib/docker/containerd
mount -t qtfs /var/lib/docker/overlay2 /another_rootfs/var/lib/docker/overlay2
mount -t qtfs /var/lib/docker/image /another_rootfs/var/lib/docker/image
mount -t qtfs /var/lib/docker/tmp /another_rootfs/var/lib/docker/tmp
mkdir -p /another_rootfs/run/containerd/io.containerd.runtime.v1.linux/
mount -t qtfs /run/containerd/io.containerd.runtime.v1.linux/ /another_rootfs/run/containerd/io.containerd.runtime.v1.linux/
mkdir -p /another_rootfs/var/run/docker/containerd
mount -t qtfs /run/docker/containerd /another_rootfs/run/docker/containerd
mkdir -p /another_rootfs/var/lib/containerd/io.containerd.runtime.v1.linux
mount -t qtfs /var/lib/containerd/io.containerd.runtime.v1.linux /another_rootfs/var/lib/containerd/io.containerd.runtime.v1.linux
```

## 3.5 拉起dockerd和containerd

在dpu上，打开两个窗口，并且都chroot到dockerd和containerd运行所需的/another_rootfs
``` bash
chroot /another_rootfs
```
在两个窗口中用如下的命令先拉起containerd，后拉起containerd

containerd
``` bash
#!/bin/bash
SHIM_HOST=<hostip> containerd --config /var/run/docker/containerd/containerd.toml --address /var/run/containerd/containerd.sock
```
执行完c
dockerd
``` bash
#!/bin/bash
rm -rf /var/lib/docker/containers/*
CMD_NET_ADDR=tcp://<dpu ip>:<dpu端rexec端口号> /usr/bin/rexec mount -t qtfs /var/lib/docker/overlay2 /root/p2/var/lib/docker/overlay2/
SHIM_HOST=<host ip> CMD_NET_ADDR=tcp://<host ip>:<host端rexec端口号> /usr/bin/dockerd --containerd /var/run/containerd/containerd.sock -s overlay2 --debug 2>&1 | tee docker.log
```
因为我们已经将`/var/run/`和`/another_rootfs/var/run/`绑定在了一起，所以，我们可以在正常的rootfs下，通过docker来访问docker.sock接口，从而管理容器。
# 4 环境恢复

如果需要卸载相关的目录，需要先杀掉shim，再关掉containerd、dockerd，并执行如下指令

``` bash
for i in `lsof | grep v1.linux | awk '{print $2}'`
do
        kill -9 $i
done
for i in `mount | grep qtfs | awk '{print $3}'`
do
        umount $i
done
umount /another_rootfs/etc
umount /another_rootfs/var/lib
umount /another_rootfs/var/run
```

