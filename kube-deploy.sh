prestart(){
echo "----prestart deploy"
cat > ca-ssl.json <<EOF
{
    "signing": {
        "default": {
            "expiry": "867240h"
        },
        "profiles": {
            "kubernetes": {
                "usages": [
                    "signing",
                    "key encipherment",
                    "server auth",
                    "client auth"
                ],
                "expiry": "867240h"
            }
        }
    }
}
EOF
cat > ca-csr.json <<EOF
{
    "CN": "kubernetes",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Guangdong",
            "L": "Zhuhai",
            "O": "k8s",
            "OU": "system"
        }
    ],
    "ca": {
        "expiry": "867240h"
    }
}
EOF
cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
EOF
cfssl gencert -initca ca-csr.json | cfssljson -bare ca
#yes | cp ca*pem /etc/kubernetes/pki
echo "----finish prestart----"
}

delpoy_etcd(){
echo "----start deloy etcd----"
cat > etcd-csr.json <<EOF
{
    "CN": "etcd",
    "hosts": [
        "127.0.0.1",
        "10.44.142.86"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Guangdong",
            "L": "Zhuhai",
            "O": "k8s",
            "OU": "system"
        }
    ]
}
EOF
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes etcd-csr.json | cfssljson -bare etcd
yes | cp etcd*pem /etc/kubernetes/pki
cat > /etc/etcd/etcd.conf <<EOF
ETCD_NAME="etcd1"
ETCD_DATA_DIR="/var/lib/etcd/default.etcd"
ETCD_LISTEN_PEER_URLS="https://0.0.0.0:2380"
ETCD_LISTEN_CLIENT_URLS="https://0.0.0.0:2379"
ETCD_INITIAL_ADVERTISE_PEER_URLS="https://0.0.0.0:2380"
ETCD_ADVERTISE_CLIENT_URLS="https://0.0.0.0:2379"
ETCD_INITIAL_CLUSTER="etcd1=https://0.0.0.0:2380"
ETCD_INITIAL_CLUSTER_TOKEN="etcd-cluster"
ETCD_INITIAL_CLUSTER_STATE="new"
EOF
cat > /lib/systemd/system/etcd.service <<EOF
[Unit]
Description=etcd server
After=network.target
After=network-online.target
Wants=network-online.target
[Service]
Type=notify
EnvironmentFile=-/etc/etcd/etcd.conf
WorkingDirectory=/var/lib/etcd
ExecStart=/usr/local/bin/etcd   --cert-file=/etc/kubernetes/pki/etcd.pem   --key-file=/etc/kubernetes/pki/etcd-key.pem   --trusted-ca-file=/etc/kubernetes/pki/ca.pem   --peer-cert-file=/etc/kubernetes/pki/etcd.pem   --peer-key-file=/etc/kubernetes/pki/etcd-key.pem   --peer-trusted-ca-file=/etc/kubernetes/pki/ca.pem   --peer-client-cert-auth   --client-cert-auth
Restart=on-failure
RestartSec=5
LimitNOFILE=65535
[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable --now etcd
echo "----finish deploy etcd----"
}
deploy_kube_apiserver(){
echo "----start deploy kube-apiserver----"
cat > kube-apiserver-csr.json << EOF
{
    "CN": "kubernetes",
    "hosts": [
        "127.0.0.1",
        "10.44.142.86",
        "10.96.0.1",
        "kubernetes",
        "kubernetes.default",
        "kubernetes.default.svc",
        "kubernetes.default.svc.cluster",
        "kubernetes.default.svc.cluster.local"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Guangdong",
            "L": "Zhuhai",
            "O": "k8s",
            "OU": "system"
        }
    ]
}
EOF
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kube-apiserver-csr.json | cfssljson -bare kube-apiserver
yes | cp kube-apiserver*pem /etc/kubernetes/pki
sudo cat > /etc/kubernetes/token.csv <<EOF
$(head -c 16 /dev/urandom | od -An -t x | tr -d ' '),kubelet-bootstrap,10001," system:bootstrapper"
EOF
cat > /etc/kubernetes/kube-apiserver.conf <<EOF
KUBE_APISERVER_OPTS="--enable-admission-plugins=NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota   --anonymous-auth=false   --bind-address=0.0.0.0   --secure-port=6443   --insecure-port=0   --authorization-mode=Node,RBAC   --runtime-config=api/all=true   --enable-bootstrap-token-auth   --service-cluster-ip-range=10.96.0.0/16   --token-auth-file=/etc/kubernetes/token.csv   --service-node-port-range=30000-32767   --tls-cert-file=/etc/kubernetes/pki/kube-apiserver.pem   --tls-private-key-file=/etc/kubernetes/pki/kube-apiserver-key.pem   --client-ca-file=/etc/kubernetes/pki/ca.pem   --kubelet-client-certificate=/etc/kubernetes/pki/kube-apiserver.pem   --kubelet-client-key=/etc/kubernetes/pki/kube-apiserver-key.pem   --service-account-key-file=/etc/kubernetes/pki/ca-key.pem   --service-account-signing-key-file=/etc/kubernetes/pki/ca-key.pem   --service-account-issuer=https://kubernetes.default.svc.cluster.local   --etcd-cafile=/etc/kubernetes/pki/ca.pem   --etcd-certfile=/etc/kubernetes/pki/etcd.pem   --etcd-keyfile=/etc/kubernetes/pki/etcd-key.pem   --etcd-servers=https://10.44.142.86:2379   --enable-swagger-ui=true   --allow-privileged=true   --apiserver-count=1   --audit-log-maxage=30   --audit-log-maxbackup=3   --audit-log-maxsize=100   --audit-log-path=/var/log/kube-apiserver-audit.log   --event-ttl=1h   --alsologtostderr=false   --log-dir=/var/log/kubernetes   --v=4"
EOF
cat > /lib/systemd/system/kube-apiserver.service <<EOF
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/kubernetes/kubernetes
After=network.target network-online.target
Wants=network-online.target
[Service]
Type=notify
EnvironmentFile=-/etc/kubernetes/kube-apiserver.conf
ExecStart=/usr/local/bin/kube-apiserver $KUBE_APISERVER_OPTS
Restart=on-failure
RestartSec=5
LimitNOFILE=65535
[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable --now kube-apiserver
echo "----finish deploy kube-apiserver"
}

init_kubectl(){
echo "----start init kubectl----"
caf > kubectl-csr.json <<EOF
kubectl-csr.json
{
    "CN": "clusteradmin",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Guangdong",
            "L": "Zhuhai",
            "O": "system:masters",
            "OU": "system"
        }
    ]
}
EOF
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kubectl-csr.json | cfssljson -bare kubectl
kubectl config set-cluster kubernetes --certificate-authority=ca.pem --embed-certs=true --server=https://10.44.142.86:6443 --kubeconfig=kube.config
kubectl config set-credentials clusteradmin --client-certificate=kubectl.pem --client-key=kubectl-key.pem --embed-certs=true --kubeconfig=kube.config
kubectl config set-context kubernetes --cluster=kubernetes --user=clusteradmin --kubeconfig=kube.config
kubectl config use-context kubernetes --kubeconfig=kube.config
mkdir -p ~/.kube
cp kube.config ~/.kube/config
kubectl cluster-info
echo "-----finish init kubectl----"
}

deploy_kube_controller_manager(){
echo "----start delpoy kube-controller-manager----"
cat > kube-controller-manager-csr.json <<EOF
{	
    "CN": "system:kube-controller-manager",
    "hosts": [
        "127.0.0.1",
        "10.44.142.86"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Guangdong",
            "L": "Zhuhai",
            "O": "system:kube-controller-manager",
            "OU": "system"
        }
    ]
}
EOF
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager
yes | cp kube-controller-manager*pem /etc/kubernetes/pki
cat > /etc/kubernetes/kube-controller-manager.conf <<EOF
KUBE_CONTROLLER_MANAGER_OPTS="   --secure-port=10257   --kubeconfig=/etc/kubernetes/kube-controller-manager.kubeconfig   --service-cluster-ip-range=10.96.0.0/16   --cluster-name=kubernetes   --cluster-signing-cert-file=/etc/kubernetes/pki/ca.pem   --cluster-signing-key-file=/etc/kubernetes/pki/ca-key.pem   --cluster-signing-duration=867240h   --tls-cert-file=/etc/kubernetes/pki/kube-controller-manager.pem   --tls-private-key-file=/etc/kubernetes/pki/kube-controller-manager-key.pem   --service-account-private-key-file=/etc/kubernetes/pki/ca-key.pem   --root-ca-file=/etc/kubernetes/pki/ca.pem   --leader-elect=true   --controllers=*,bootstrapsigner,tokencleaner   --use-service-account-credentials=true   --horizontal-pod-autoscaler-sync-period=10s   --alsologtostderr=true   --logtostderr=false   --log-dir=/var/log/kubernetes   --allocate-node-cidrs=true   --cluster-cidr=10.244.0.0/12   --v=4"
EOF
kubectl config set-cluster kubernetes --certificate-authority=ca.pem --embed-certs=true --server=https://10.44.142.86:6443 --kubeconfig=kube-controller-manager.kubeconfig
kubectl config set-credentials kube-controller-manager --client-certificate=kube-controller-manager.pem --client-key=kube-controller-manager-key.pem --embed-certs=true --kubeconfig=kube-controller-manager.kubeconfig
kubectl config set-context default --cluster=kubernetes --user=kube-controller-manager --kubeconfig=kube-controller-manager.kubeconfig
kubectl config use-context default --kubeconfig=kube-controller-manager.kubeconfig
yes | cp kube-controller-manager.kubeconfig /etc/kubernetes/

cat > /lib/systemd/system/kube-controller-manager.service <<EOF
[Unit]
Description=Kubernetes controller manager
Documentation=https://github.com/kubernetes/kubernetes
After=network.target network-online.target
Wants=network-online.target
[Service]
EnvironmentFile=-/etc/kubernetes/kube-controller-manager.conf
ExecStart=/usr/local/bin/kube-controller-manager $KUBE_CONTROLLER_MANAGER_OPTS
Restart=on-failure
RestartSec=5
LimitNOFILE=65535
[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable --now kube-controller-manager
echo "----finish delpoy kube-controller-manager----"
}

deploy_kube_scheduler(){
echo "----start deploy kube scheduler"
cat > kube-scheduler-csr.json <<EOF
{
    "CN": "system:kube-scheduler",
    "hosts": [
        "127.0.0.1",
        "10.44.142.86"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Guangdong",
            "L": "Zhuhai",
            "O": "system:kube-scheduler",
            "OU": "system"
        }
    ]
}
EOF
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kube-scheduler-csr.json | cfssljson -bare kube-scheduler
yes | cp kube-scheduler*pem /etc/kubernetes/pki
cat > /etc/kubernetes/kube-scheduler.conf <<EOF
KUBE_SCHEDULER_OPTS="--address=127.0.0.1   --kubeconfig=/etc/kubernetes/kube-scheduler.kubeconfig   --leader-elect=true   --alsologtostderr=true   --logtostderr=false   --log-dir=/var/log/kubernetes   --v=4"
EOF
kubectl config set-cluster kubernetes --certificate-authority=ca.pem --embed-certs=true --server=https://10.44.142.86:6443 --kubeconfig=kube-scheduler.kubeconfig
kubectl config set-credentials kube-scheduler --client-certificate=kube-scheduler.pem --client-key=kube-scheduler-key.pem --embed-certs=true --kubeconfig=kube-scheduler.kubeconfig
kubectl config set-context default --cluster=kubernetes --user=kube-scheduler --kubeconfig=kube-scheduler.kubeconfig
kubectl config use-context default --kubeconfig=kube-scheduler.kubeconfig
yes | cp kube-scheduler.kubeconfig /etc/kubernetes/
cat > /lib/systemd/system/kube-scheduler.service <<EOF
[Unit]
Description=Kubernetes scheduler
Documentation=https://github.com/kubernetes/kubernetes
After=network.target network-online.target
Wants=network-online.target
[Service]
EnvironmentFile=-/etc/kubernetes/kube-scheduler.conf
ExecStart=/usr/bin/kube-scheduler $KUBE_SCHEDULER_OPTS
Restart=on-failure
RestartSec=5
LimitNOFILE=65535
[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable --now kube-scheduler
echo "----finish delpoy kube scheduler----"
}

deploy_kubelet(){
cat > kubelet-csr.json <<EOF
{
    "CN": "system:kubelet",
    "hosts": [
        "127.0.0.1",
        "10.44.142.86"
    ],
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "ST": "Guangdong",
            "L": "Zhuhai",
            "O": "system:kubelet",
            "OU": "system"
        }
    ]
}
EOF
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kubelet-csr.json | cfssljson -bare kubelet
yes | cp kubelet*pem /etc/kubernetes/pki
cat > /etc/kubernetes/kubelet.conf <<EOF
KUBELET_OPTS="--config=/etc/kubernetes/kubelet.yaml   --kubeconfig=/etc/kubernetes/kubelet.kubeconfig   --cert-dir=/etc/kubernetes/pki--network-plugin=cni  --logtostderr=false   --v=4   --log-dir=/var/log/kubernetes   --fail-swap-on=false --container-runtime=remote --container-runtime-endpoint=/var/run/docker.sock"
EOF
cat > /etc/kubernetes/kubelet.yaml <<EOF
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
address: 0.0.0.0
port: 10250
readOnlyPort: 0
authentication:
  anonymous:
    enabled: false
  webhook:
    cacheTTL: 2m0s
    enabled: true
  x509:
    clientCAFile: /etc/kubernetes/pki/ca.pem
authorization:
  mode: Webhook
  webhook:
    cacheAuthorizedTTL: 5m0s
    cacheUnauthorizedTTL: 30s
cgroupDriver: systemd
clusterDNS:
- 10.44.142.86
clusterDomain: cluster.local
healthzBindAddress: 127.0.0.1
healthzPort: 10248
rotateCertificates: true
evictionHard:
  imagefs.available: 15%
  memory.available: 100Mi
  nodefs.available: 10%
  nodefs.inodesFree: 5%
maxOpenFiles: 1000000
maxPods: 110
EOF
kubectl config set-cluster kubernetes --certificate-authority=ca.pem --embed-certs=true --server=https://10.44.142.86:6443 --kubeconfig=kubelet.kubeconfig
kubectl config set-credentials kubelet --client-certificate=kubelet.pem --client-key=kubelet-key.pem --embed-certs=true --kubeconfig=kubelet.kubeconfig
kubectl config set-context default --cluster=kubernetes --user=kubelet --kubeconfig=kubelet.kubeconfig
kubectl config use-context default --kubeconfig=kubelet.kubeconfig
yes | cp kubelet.kubeconfig /etc/kubernetes/
cat > /lib/systemd/system/kubelet.service <<EOF
[Unit]
Description=Kubernetes kubelet
After=network.target network-online.targer docker.service
Wants=docker.service
[Service]
EnvironmentFile=-/etc/kubernetes/kubelet.conf
ExecStart=/usr/local/bin/kubelet $KUBELET_OPTS
Restart=on-failure
RestartSec=5
LimitNOFILE=65535
[Install]
WantedBy=multi-user.target
EOF
}

create_csr(){
cat > role.yaml <<EOF
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
        name: system:node-bootstrapper
subjects:
- kind: Group
  name: system:bootstrappers
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: system:node-bootstrapper
  apiGroup: rbac.authorization.k8s.io
EOF
kubectl create -f role.yaml
kubectl create clusterrolebinding kubelet --clusterrole=cluster-admin --user=system:kubelet --group=certificates.k8s.io
}

prestart
delpoy_etcd
deploy_kube_apiserver
init_kubectl
deploy_kube_controller_manager
deploy_kube_scheduler
deploy_kubelet
create_csr
